import React from 'react';
import ReactDOM from 'react-dom';
import Home from '../home-template/containers/home-container';
import Contact from '../contact-template/containers/contact-container';
import Layout from '../layout';
import { Router, Route, IndexRoute } from 'react-router';

export default(
    <Route path="/" component={Layout}>
      <IndexRoute component={Home} />
      <Route path='/home' component={Home}></Route>
      <Route path='/contact' component={Contact}></Route>
    </Route>
);
