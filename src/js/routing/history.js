import React from 'react';
import ReactDOM from 'react-dom';
import createHashHistory from 'history/lib/createHashHistory';
import {Router, Route, IndexRoute} from 'react-router';
import App from '../app';

export default createHashHistory();
