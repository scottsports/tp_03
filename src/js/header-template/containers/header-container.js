import { connect } from 'react-redux';
import Header from '../components/header-component';

const mapStateToProps = () => {
  return {
  }
};

const mapDispatchToProps = (dispatch)=>{
  return {
  }
};

const HeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

export default HeaderContainer
