import React from 'react';
import classNames from 'classnames';
import { Link } from 'react-router';{ /*si pas de crochés import tout le router*/}

export default class Header extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-default" role="navigation">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
        </div>

        <div className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li><Link activeClassName='active' to="/home">Home</Link></li>
            <li><Link activeClassName='active' to="/contact">Contact-us</Link></li>
          </ul>
        </div>
      </nav>
    );
  }

}
