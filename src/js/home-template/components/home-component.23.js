import React from 'react';
import ReactDOM from 'react-dom';
import uuid from 'uuid';


export default class Home extends React.Component {

  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }



  render() {

    return (
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">I'm happy to lean GIT!</h5>
              <h6 className="card-subtitle mb-2 text-muted">Bootstrap 4.0.0 Snippet by pradeep330</h6>
              <p className="card-text"><iframe src="https://giphy.com/embed/xHMIDAy1qkzNS" width="480" height="355" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/thumbs-up-xHMIDAy1qkzNS"></a></p></p>
              <a href="https://bootsnipp.com/pradeep330" className="card-link">link</a>
              <a href="http://websitedesigntamilnadu.com" className="card-link">Another link</a>
            </div>
          </div>
    );
  }
}
