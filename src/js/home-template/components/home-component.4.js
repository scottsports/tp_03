import React from 'react';
import ReactDOM from 'react-dom';
import uuid from 'uuid';


export default class Home extends React.Component {

  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }



  render() {

    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">WebTeam</h5>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Greg</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Emilien</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Flora</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Joël</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Eleni</td>
              </tr>
              <tr>
                <td>6</td>
                <td>Mike</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Adriano</td>
              </tr>
              <tr>
                <td>8</td>
                <td>Didier</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
