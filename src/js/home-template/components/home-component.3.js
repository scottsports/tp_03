import React from 'react';
import ReactDOM from 'react-dom';
import uuid from 'uuid';


export default class Home extends React.Component {

  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }



  render() {

    return (
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Try Other</h5>
              <h6 className="card-subtitle mb-2 text-muted">Bootstrap 4.0.0 Snippet by pradeep330</h6>
              <p className="card-text">You can also try different version of Bootstrap V4 side menu. Click below link to view all Bootstrap Menu versions.</p>
              <a href="https://bootsnipp.com/pradeep330" className="card-link">link</a>
              <a href="http://websitedesigntamilnadu.com" className="card-link">Another link</a>
            </div>
          </div>
    );
  }
}
