import React from 'react';
import ReactDOM from 'react-dom';
import uuid from 'uuid';
import Component_1 from './home-component.1';
import Component_2 from './home-component.2';
import Component_3 from './home-component.3';
import Component_21 from './home-component.21';
import Component_22 from './home-component.22';
import Component_23 from './home-component.23';
import Component_4 from './home-component.4';

export default class Home extends React.Component {

  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }



  render() {

    return (
      <div>
        <div className="row">
          <div className="col-lg-4">
            < Component_1 />
          </div>
          <div className="col-lg-4">
            < Component_2 />
          </div>
          <div className="col-lg-4">
            < Component_3 />
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-lg-4">
            < Component_21 />
          </div>
          <div className="col-lg-4">
            < Component_22 />
          </div>
          <div className="col-lg-4">
            < Component_23 />
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-lg-12">
            < Component_4 />
          </div>
        </div>
      </div>
    );
  }
}
