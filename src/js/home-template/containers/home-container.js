import { connect } from 'react-redux';
import { homeAction }from '../actions/home-actions';
import Home from '../components/home-component';

const mapStateToProps = (state) => {
  return {
  }
};

const mapDispatchToProps = (dispatch)=>{
  return {
    addExpense: (expense) => {
      dispatch(addExpense(expense));
    }
  }
};

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

export default HomeContainer
