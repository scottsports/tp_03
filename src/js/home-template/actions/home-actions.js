import superagent from 'superagent';

export const HOME_ACTION = 'HOME_ACTION';

export function hommeAction(expense) {

  return (dispatch) => {
    superagent.post('/rest/expense')
      .set('Accept', 'application/json')
      .body(expense)
      .end((err, res) => {
        if (res.ok) {
          dispatch({
            type: HOME_ACTION,
            etat: res.ok
          })
        }
      });
  }
}
