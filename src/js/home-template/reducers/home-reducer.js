import { HOME_ACTION } from'../actions/home-actions';
import { findIndex } from  'lodash'

const initialState = [];
 
export default function homeReducer(state = initialState, action){

  switch (action.type) {
    case HOME_ACTION:{
      const newState = {...state, etat: action.etat}
      return  newState;
    }
    default:
      return state;
  }
  
}
