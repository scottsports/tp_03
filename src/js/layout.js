import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header-template/containers/header-container';
{/*prend les props en entree*/ }
const Layout = ({ children }) => (
  <div>
    <Header />
    <div>
      {children}
    </div>
  </div>
);
export default Layout

{/*equivaut à
const Layout = (props) => (
      <div>
        <Header />
        <div className="container-fluid">
          <div className="container">
            {props.children}
          </div>
        </div>
      </div>
    );
export default Layout
*/}
