import thunkMiddleware from 'redux-thunk';
import  { createStore, applyMiddleware, compose,combineReducers } from 'redux';
import homeReducer from '../home-template/reducers/home-reducer';
import contactReducer from '../contact-template/reducers/contact-reducer';

const reducersCombined = combineReducers({homeReducer,contactReducer});

const store = createStore(
  reducersCombined,
  compose(
    applyMiddleware(
      thunkMiddleware,//,aide pour git ne pas utiliser sur ie, edge ok
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);
export default store
