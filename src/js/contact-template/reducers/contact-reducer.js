import { CONTACT_ACTION } from'../actions/contact-actions';
import { findIndex } from  'lodash'

const initialState = [];
 
export default function contactReducer(state = initialState, action){

  switch (action.type) {
    case CONTACT_ACTION:{
      const newState = {...state, etat: action.etat}
      return  newState;
    }
    default:
      return state;
  }
  
}
