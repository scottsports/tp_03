import superagent from 'superagent';

export const CONTACT_ACTION = 'CONTACT_ACTION';

export function contactAction(expense) {

  return (dispatch) => {
    superagent.post('/rest/expense')
      .set('Accept', 'application/json')
      .body(expense)
      .end((err, res) => {
        if (res.ok) {
          dispatch({
            type: CONTACT_ACTION,
            etat: res.ok
          })
        }
      });
  }
}
