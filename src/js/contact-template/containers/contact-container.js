import { connect } from 'react-redux';
import { contactAction }from '../actions/contact-actions';
import Contact from '../components/contact-component';

const mapStateToProps = (state) => {
  return {
  }
};

const mapDispatchToProps = (dispatch)=>{
  return {
    addExpense: (expense) => {
      dispatch(addExpense(expense));
    }
  }
};

const ContactContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);

export default ContactContainer
