/**
 * expenses API.
 * GET /rest/tirages
 * GET /rest/tirage/:id
 * POST /rest/tirage
 * PUT /rest/tirage/:id
 */

 /**
  * Modifier selon les besoins 
  */
var _ = require('lodash');
var tirages = require('./tirages');

module.exports = function (app) {
  var id = _.last(tirages).id;


  app.get('/rest/tirages', function(req, res) {
    console.log('GET /rest/tirages');
    return res.status(200).json(tirages);
  });

  app.get('/rest/tirages/:id', function(req, res) {
    console.log('GET /rest/tirages/' + req.params.id);

    var id = Number(req.params.id);
    var tirage = _.find(tirages, function (t) {
      return t.id === id;
    });

    if (!tirage) {
      return res.status(404).send();
    } else {
      return res.status(200).json(tirage);
    }
  });

  app.post('/rest/tirage', function(req, res) {
    console.log('POST /rest/tirage');
    console.log('  => body: ', req.body);

    var body = req.body;

    if (!body || _.has(body, 'id')) {
      return res.status(400).send();
    }

    body.id = ++id;
    body = _.defaults(body, {});

    tirages.push(body);

    return res.status(201).json(body);
  });

  app.put('/rest/tirage/:id', function(req, res) {
    console.log('PUT /rest/tirage/' + req.params.id);
    console.log('  => body: ', req.body);

    var body = req.body;

    if (!body) {
      return res.status(400).send();
    }

    var id = Number(req.params.id);
    var tirage = _.find(tirages, function (t) {
      return t.id === id;
    });

    if (tirage) {
      _.extend(tirage, body);
    } else {
      tirage = body;
      tirage.id = id;
    }

    return res.status(200).json(tirage);
  });
};